import 'package:new_sec_0_3/data/repository/requests.dart';
import 'package:new_sec_0_3/domain/utils.dart';

class SignInUseCase{
Future<void> pressbutton(
    String email,
    String password,
    Function(void) onResponse,
    Future<void> Function(String) onError

    )async{

    requestSignIn() async{
      await signIn(email: email, password: password);
    }
    await requests(requestSignIn, onResponse, onError);

}
}