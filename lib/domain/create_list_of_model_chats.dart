import 'package:new_sec_0_3/data/models/modelchat.dart';
import 'package:new_sec_0_3/data/models/modelprofile.dart';
import 'package:new_sec_0_3/data/repository/requests.dart';
import 'package:new_sec_0_3/main.dart';

Future<List<ModelChat>> createListofModelChat()async{
  final data = await getAllChatsofUser();
  List<ModelChat> result = [];
  for (int i =0; i <data.length; i++){
    final profileData = await getAnotherProfile((data[i]['first_user_id'] == supabase.auth.currentUser!.id)?data[i]['second_user_id']:data[i]['first_user_id']);
    final lastMessage = await getLastMessage(data[i]['id']);
    result.add(ModelChat(
        id: data[i]['id'],
        profile: ModelProfile(fullname: profileData['fullname'], balance: profileData['balance'].toString(), phone: profileData['phone'].toString(), id: profileData['id_user']),
        lastMessage: {'message': lastMessage['message'], 'created_at': DateTime.parse(lastMessage['created_at'])}));
  }
  result.sort((a, b) => a.lastMessage['created_at'].compareTo(b.lastMessage['created_at']));
  return result.reversed.toList();
}

