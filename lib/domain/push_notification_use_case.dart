import 'package:new_sec_0_3/data/models/modelmessage.dart';
import 'package:timezone/data/latest_all.dart' as tz;
import 'package:timezone/timezone.dart' as tz;
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class NotificationUseCase {

  Function(String message) onReceiveMessage;

  NotificationUseCase(this.onReceiveMessage);

  var plugin = FlutterLocalNotificationsPlugin();

  Future<bool> requestPermission() async {
    return await plugin.resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()
        ?.requestNotificationsPermission() ?? false;
  }

  Future<bool> init() async {
    tz.initializeTimeZones();
    tz.setLocalLocation(tz.getLocation("Europe/Moscow"));
    return await plugin.initialize(
        const InitializationSettings(
            android: AndroidInitializationSettings("app_icon")
        ),
    ) ?? false;
  }

  void pushNotification(ModelMessage message){
    plugin.show(
        0, 'New message', message.message,
        const NotificationDetails(
            android: AndroidNotificationDetails(
                "channel-id", "channel-name",
                importance: Importance.max,
                priority: Priority.high,
                color: Colors.blue
            )
        ),
        payload: "test payload"
    );
  }

}