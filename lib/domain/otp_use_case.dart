import 'package:new_sec_0_3/data/repository/requests.dart';
import 'package:new_sec_0_3/domain/utils.dart';

class OtPUseCase{
  Future<void> pressbutton(
      String email,
      String code,
      Function(void) onResponse,
      Future<void> Function(String) onError

      )async{

    requestOTP() async{
      await otp(email: email, code: code);
    }
    await requests(requestOTP, onResponse, onError);

  }
}