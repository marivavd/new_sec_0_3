import 'dart:typed_data';

import 'package:image_picker/image_picker.dart';
import 'package:new_sec_0_3/data/repository/requests.dart';

class AvatarUseCase{
  XFile? avatar;
  final Future<ImageSource?> Function() onChooseSource;
  final Function(Uint8List) onPickAvatar;
  final Function() onRemoveAvatar;

  AvatarUseCase({
    required this.onRemoveAvatar,
    required this.onPickAvatar,
    required this.onChooseSource
});

  void pressButton(flag){
    if (flag){
      pressRemoveButton();
      flag = false;
    }else{

      pressChangeButton();
      flag = true;
    }
  }

  Future<void> pressChangeButton()async{
    var imageSource = await onChooseSource();
    if (imageSource == null){
      return;
    }
    avatar = (await ImagePicker().pickImage(source: imageSource));
    var bytes = await avatar?.readAsBytes();
    if (bytes != null) {
      uploadAvatar(bytes);
    onPickAvatar(bytes);

    }
  }


  void pressRemoveButton(){
    avatar = null;
    removeAvatar();
    onRemoveAvatar();
  }

}