import 'package:new_sec_0_3/data/models/modelchat.dart';
import 'package:new_sec_0_3/data/models/modelmessage.dart';

List<ModelMessage> getPushMessages(List<ModelMessage> oldSp, List<ModelMessage> newSp){
  List<ModelMessage> result = [];
  for (int i = 0; i < newSp.length; i++){
    bool flag = true;
    for (int u = 0; u <oldSp.length; u++){
      if (oldSp[u].id == newSp[i].id){
        flag = false;
      }
    }
    if (flag){
      result.add(newSp[i]);
    }
  }
  return result;
}

Map<String, int> getNotReadenMessages(List<ModelMessage> pushmessages, List<ModelChat> currentchats, Map<String, int> oldNotreadenchats){
  Map<String, int> result = {};
  for (int i = 0; i < currentchats.length; i++){
    result[currentchats[i].id] = oldNotreadenchats[currentchats[i].id]!;
    for (var val in pushmessages){
      if (val.idChat == currentchats[i].id){
        result[currentchats[i].id] = 1+ oldNotreadenchats[currentchats[i].id]!;
      }
    }
  }
  return result;
}