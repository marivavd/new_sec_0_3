import 'package:new_sec_0_3/data/models/modelprofile.dart';

List<ModelProfile> searchProfiles(List<ModelProfile> profiles, String text){
  List<ModelProfile> result = [];
  for (var val in profiles){
    if (val.fullname.toLowerCase().contains(text.toLowerCase())){
      result.add(val);
    }
  }
  return result;
}