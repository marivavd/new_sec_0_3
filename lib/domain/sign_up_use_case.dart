import 'package:new_sec_0_3/data/repository/requests.dart';
import 'package:new_sec_0_3/domain/utils.dart';

class SignUpUseCase{
  Future<void> pressbutton(
      String fullname,
      String phone,
      String email,
      String password,
      String confPassword,
      Function(void) onResponse,
      Future<void> Function(String) onError

      )async{
    if (password != confPassword){
      onError('Passwords dont match');
    }
    else{
      requestSignUp() async{
        await signUp(email: email, password: password, fullname: fullname, phone: phone);
      }
      await requests(requestSignUp, onResponse, onError);
    }

  }
}