import 'package:new_sec_0_3/data/repository/requests.dart';

Future<List<String>> createListAdsUrl()async{
  final data = await get_ads();
  List<String> result = [];
  for (int i =0; i <data.length; i++){
    result.add(data[i]['ads_url']);
  }
  return result;
}