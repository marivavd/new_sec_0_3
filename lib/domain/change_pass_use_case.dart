import 'package:new_sec_0_3/data/repository/requests.dart';
import 'package:new_sec_0_3/domain/utils.dart';

class ChangePassUseCase{
  Future<void> pressbutton(
      String password,
      String confPassword,
      Function(void) onResponse,
      Future<void> Function(String) onError

      )async{
    if (password != confPassword){
      onError('Passwords dont match');
    }
    else{
      requestChangePass() async{
        await changePass(password: password);
      }
      await requests(requestChangePass, onResponse, onError);
    }

  }
}