import 'package:flutter/material.dart';
import 'package:new_sec_0_3/data/models/modelorder.dart';
import 'package:new_sec_0_3/data/repository/requests.dart';

Future<void> makeOrder(
{
  required String others,
  required String phone,
  required String country,
  required String address,
  required String items,
  required String weight,
  required String worth,
  required List<TextEditingController> adresses,
  required List<TextEditingController> phones,
  required List<TextEditingController> countries,
  required List<TextEditingController> otherses,
  required Function(ModelOrder) onResponse,
  required Function(String) onError


}
    )async{
  List<Map<String, String>> destinations = [];
  for (int i = 0; i < adresses.length; i++){
    destinations.add(
        {
          "address": adresses[i].text,
          "country": countries[i].text,
          "phone": phones[i].text,
          "others": otherses[i].text,
          "id_order": ''
        }
    );
  }
  try{
    double delivery_charges = adresses.length * 2500;
    double instantDelivery = 300.00;
    double tax = (delivery_charges + instantDelivery) * 0.05;
    double sum_price = tax + instantDelivery + delivery_charges;

    String idOrder = await insertOrder(others: others, phone: phone, country: country, instantDelivery: instantDelivery, address: address, items: items, weight: int.parse(weight), worth: int.parse(worth), delivery_charges: delivery_charges, sum_price: sum_price, tax: tax, destinations: destinations);
    onResponse(ModelOrder(id: idOrder, phone: phone, sum_price: sum_price, tax: tax, delivery_charges: delivery_charges, worth: int.parse(worth.toString()), weight: int.parse(weight.toString()), items: items, country: country, address: address, instantDelivery: instantDelivery, others: others));

  }
  catch (e){
    onError(e.toString());
  }

}