import 'package:new_sec_0_3/data/repository/requests.dart';
import 'package:new_sec_0_3/domain/utils.dart';

class SendEmailUseCase{
  Future<void> pressbutton(
      String email,
      Function(void) onResponse,
      Future<void> Function(String) onError

      )async{

    requestSend() async{
      await sendEmail(email: email);
    }
    await requests(requestSend, onResponse, onError);

  }
}