import 'package:geolocator/geolocator.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class GeoUseCase{
  Future<void> getCurrentLocation({
    required Function(Position) onResponse,
    required Function(String) onError
})async{
    LocationPermission permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied){
      permission = await Geolocator.requestPermission();
    }
    if(!(await Geolocator.isLocationServiceEnabled())){
      onError('Location servise disable');
      return;
    }
    if (permission == LocationPermission.deniedForever || permission == LocationPermission.denied){
      onError('Permision denied');
      return;
    }
    var position = await Geolocator.getCurrentPosition();
    onResponse(position);
  }

  Future<void> getCurrentAddress(
      Point point,
      {
    required Function(String) onResponse,
    required Function(String) onError
})async{
    var response = await YandexSearch.searchByPoint(
        point: point,
        searchOptions: SearchOptions(
          searchType: SearchType.geo,
          geometry: false
        ));
    var result = await response.result;
    var firstResult = result.items?.firstOrNull?.name;
    if (firstResult == null){
      onError('Cant find address');
      return;
    }
    onResponse(firstResult);

}


}