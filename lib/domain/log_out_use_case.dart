import 'package:new_sec_0_3/data/repository/requests.dart';
import 'package:new_sec_0_3/domain/utils.dart';

class LogOutUseCase{
Future<void> pressbutton(
    Function(void) onResponse,
    Future<void> Function(String) onError

    )async{

  requestLogOut() async{
    await logOut();
  }
  await requests(requestLogOut, onResponse, onError);

}
}