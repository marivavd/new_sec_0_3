
import 'package:new_sec_0_3/data/repository/requests.dart';

Future<String> createDestText(String idOrder)async{
  final destinations = await getDestinationDetailsOrder(idOrder);
  var elements = destinations.map(
          (e) => e["address"]! + ", " + e["country"]! + "\n" + e["phone"]!
  ).toList();
  var elements_with_index = [];
  for (int index = 0; index < elements.length; index++){
    elements_with_index.add("${index+1}. ${elements[index]}");
  }
  return elements_with_index.join("\n");
}