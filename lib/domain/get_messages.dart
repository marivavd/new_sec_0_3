import 'package:new_sec_0_3/data/models/modelmessage.dart';
import 'package:new_sec_0_3/data/repository/requests.dart';

Future<List<ModelMessage>> getMessages(String idUser)async{
  var chats = await get_chats(idUser);
  if (chats.length == 0){
    chats = await insert_chat(idUser);
  }
  else{
    final result = await get_messages(chats[0]['id']);
    List<ModelMessage> messages = [];
    for (int i = 0; i < result.length; i ++){
      messages.add(ModelMessage(message: result[i]['message'], idUser: result[i]['id_user'], idChat: result[i]['id_chat'], id: result[i]['id']));
    }
    return messages;
  }
  return [];
}

Future<String> getIdChat(String idUser)async{
  var chats = await get_chats(idUser);
  return chats[0]['id'];
}

Future<List<ModelMessage>> getAllMessagestoProfile()async{
  var chats = await getAllChatsofUser();

    List<ModelMessage> messages = [];
    for (int i = 0; i < chats.length; i ++){
      final result = await get_messages(chats[i]['id']);
      for (int u = 0; u <result.length; u ++){
      messages.add(ModelMessage(message: result[u]['message'], idUser: result[u]['id_user'], idChat: result[u]['id_chat'], id: result[u]['id']));}
    }
    return messages;
}