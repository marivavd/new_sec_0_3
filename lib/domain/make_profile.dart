import 'package:new_sec_0_3/data/models/modelprofile.dart';
import 'package:new_sec_0_3/data/repository/requests.dart';

Future<ModelProfile> makeProfile()async{
  final data = await getProfile();
  return ModelProfile(fullname: data['fullname'], balance: data['balance'].toString(), avatar: await getMyAvatar(), phone: data['phone'].toString(), id: data['user_id'].toString());
}

Future<List<ModelProfile>> makeListProfiles()async{
  final data = await get_users_with_rider();
  List<ModelProfile> result = [];
  for(int i = 0; i <data.length; i ++){
    result.add(ModelProfile(fullname: data[i]['fullname'], balance: data[i]['balance'].toString(), avatar: await getAvatarofProfile(data[i]['id_user']), phone: data[i]['phone'].toString(), id: data[i]['id_user'].toString()));
  }
  return result;
}