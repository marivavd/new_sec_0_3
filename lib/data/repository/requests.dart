import 'dart:typed_data';

import 'package:new_sec_0_3/main.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> signUp(
{required String email,
required String password,
required String fullname,
required String phone,
})async{
  final AuthResponse res = await supabase.auth.signUp(
    email: email,
    password: password,
  );
  await supabase
      .from('profiles')
      .insert({'fullname': fullname, 'phone': phone, "id_user": supabase.auth.currentUser!.id, 'avatar': '', 'balance': 0.0});

}

Future<void> signIn(
    {required String email,
      required String password,
    })async{
  final AuthResponse res = await supabase.auth.signInWithPassword(
    email: email,
    password: password,
  );

}

Future<void> logOut()async{
  await supabase.auth.signOut();

}

Future<void> sendEmail(
    {required String email,
    })async{
  return await supabase.auth.resetPasswordForEmail(email);

}

Future<void> otp(
    {required String email,
      required String code,
    })async{
  final AuthResponse res = await supabase.auth.verifyOTP(
    type: OtpType.email,
    token: code,
    email: email,
  );

}


Future<void> changePass(
    {
      required String password,
    })async{
  final UserResponse res = await supabase.auth.updateUser(
    UserAttributes(
      password: password
    ),
  );

}

Future<Map<String, dynamic>> getProfile()async{
  return await supabase.from('profiles').select().eq('id_user', supabase.auth.currentUser!.id).single();
}

Future<String> insertOrder(
{required String others,
  required String phone,
  required String country,
  required double instantDelivery,
  required String address,
  required String items,
  required int weight,
  required  int worth,
  required double delivery_charges,
  required double sum_price,
  required double tax,
  required List<Map<String, String>> destinations}
)async{
  String id = await supabase.from('orders').insert(
    {
      "id_user": supabase.auth.currentUser!.id,
      'others': others,
      'phone': phone,
      'country': country,
      'instant_delivery': instantDelivery,
      "address": address,
      'package_items': items,
      "weight_items": weight,
      'worth_items':worth,
      'delivery_charges': delivery_charges,
      'sum_price': sum_price,
      'tax_and_service_charges': tax
    }
  ).select().single().then((value) => value['id']);
  for (var d in destinations) {
    d["id_order"] = id;
    await supabase.from("destinations_details").insert(d).select().single();
  }
  return id;


}

Future<List<Map<String, dynamic>>> getDestinationDetailsOrder(String id_order) async{
  return await supabase.from("destinations_details").select().eq("id_order", id_order);
}

void subsribeMessage(callback){
  supabase
      .channel("changes")
      .onPostgresChanges(
      event: PostgresChangeEvent.insert,
      schema: "public",
      table: "messages",
      callback: (payload) {
        callback(payload.newRecord);
      }
  )
      .subscribe();
}
Future<dynamic> getMyAvatar()async{
  final data = await supabase.from('profiles').select().eq('id_user', supabase.auth.currentUser!.id).single();
  if (data['avatar'] != null && data['avatar'] != '' ){
    var file = await supabase.storage.from('avatars').download("${supabase.auth.currentUser!.id}.png");
    return file;
  }
  return null;
}
Future<List<Map<String, dynamic>>> get_ads() async{
  return await supabase.
  from("ads").
  select();
}


Future<List<Map<String, dynamic>>> get_users_with_rider() async{
  final rider = await supabase.from('profiles').select().eq('id_user', supabase.auth.currentUser!.id).single().then((value) => value['rider']);
  return await supabase.
  from("profiles").
  select().eq("rider", !rider);
}

Future<dynamic> getAvatarofProfile(String idUser)async{
  final data = await supabase.from('profiles').select().eq('id_user', idUser).single();
  if (data['avatar'] != null && data['avatar'] != '' ){
    var file = await supabase.storage.from('avatars').download(supabase.auth.currentUser!.userMetadata!['avatar']);
    return file;
  }
  return null;
}

Future<List> get_chats(String user_id) async{
  var chats = [];
  var sp_1 = await supabase.
  from("chats").
  select().eq("first_user_id", supabase.auth.currentUser!.id);
  var sp_2 = await supabase.
  from("chats").
  select().eq("second_user_id", supabase.auth.currentUser!.id);
  for (var val in sp_1){
    if (val['second_user_id'] == user_id){
      chats.add(val);
    }
  }
  for (var val in sp_2){
    if (val['first_user_id'] == user_id){
      chats.add(val);
    }
  }
  return chats;

}

Future<List> insert_chat(String user_id) async{
  return await supabase
      .from('chats')
      .insert({
    "first_user_id": supabase.auth.currentUser!.id,
    "second_user_id": user_id,
  }).select();

}
Future<List<Map<String, dynamic>>> get_messages(String id_chat) async{
  return await supabase.
  from("messages").
  select().
  eq("id_chat", id_chat).
  order('created_at', ascending: true);

}
Future<void> insertMessage(String message, String idChat)async{
  await supabase.from('messages').insert(
      {
        "id_user": supabase.auth.currentUser!.id,
        'id_chat': idChat,
        'message': message

      }
  );
}

Future<List> getAllChatsofUser() async{
  var chats = [];
  var sp_1 = await supabase.
  from("chats").
  select().eq("first_user_id", supabase.auth.currentUser!.id);
  var sp_2 = await supabase.
  from("chats").
  select().eq("second_user_id", supabase.auth.currentUser!.id);
  for (var val in sp_1){
      chats.add(val);
  }
  for (var val in sp_2){
      chats.add(val);
  }
  return chats;

}

Future<Map<String, dynamic>> getLastMessage(String id_chat) async{
  final data =  await supabase.
  from("messages").
  select().
  eq("id_chat", id_chat).
  order('created_at', ascending: false);
  if (data.length == 0){
    return {};
  }
  return data[0];

}

Future<Map<String, dynamic>> getAnotherProfile(String idUser)async{
  return await supabase.from('profiles').select().eq('id_user', idUser).single();
}

Future<void> uploadAvatar(Uint8List bytes) async {
  var name = "${supabase.auth.currentUser!.id}.png";
  await supabase.storage
      .from("avatars")
      .uploadBinary(
      name,
      bytes
  );
  await supabase
      .from('profiles')
      .update({ 'avatar': name })
      .eq('id_user', supabase.auth.currentUser!.id);
}

Future<void> removeAvatar() async {
  await supabase.storage
      .from("avatars")
      .remove(["${supabase.auth.currentUser!.id}.png"]);
  await supabase
      .from('profiles')
      .update({ 'avatar': '' })
      .eq('id_user', supabase.auth.currentUser!.id);
}
