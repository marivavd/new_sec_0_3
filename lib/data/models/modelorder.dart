class ModelOrder {
  String id;
  int status;
  String address;
  String country;
  String phone;
  String others;
  String items;
  int weight;
  int worth;
  double delivery_charges;
  double instantDelivery;
  double tax;
  double sum_price;
  ModelOrder({required this.id, this.status = 0, required this.others, required this.phone, required this.country, required this.instantDelivery, required this.address, required this.items, required this.weight, required this.worth, required this.delivery_charges, required this.sum_price, required this.tax});
}