import 'package:new_sec_0_3/data/models/modelprofile.dart';

class ModelChat{
  String id;
  ModelProfile profile;
  Map<String, dynamic> lastMessage;

  ModelChat({required this.id, required this.profile, required this.lastMessage});

}