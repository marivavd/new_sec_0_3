class ModelMessage{
  String message;
  String idUser;
  String idChat;
  String id;

  ModelMessage({required this.message, required this.idUser, required this.idChat, required this.id});

}