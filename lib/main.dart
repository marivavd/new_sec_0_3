import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:new_sec_0_3/presentation/dialog.dart';
import 'package:new_sec_0_3/presentation/pages/map.dart';
import 'package:new_sec_0_3/presentation/pages/sign_up.dart';
import 'package:new_sec_0_3/presentation/pages/push_not.dart';
import 'package:new_sec_0_3/presentation/theme/theme.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> main() async {
  await Supabase.initialize(
    url: 'https://uboklrrvwysdoztwfvuj.supabase.co',
    anonKey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InVib2tscnJ2d3lzZG96dHdmdnVqIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDcyOTI1MjEsImV4cCI6MjAyMjg2ODUyMX0.snO7gjARZRw677ToKsGYc49lgiHIU7HCaCw6M7rIXwY',
  );

  runApp(MyApp());
}


final supabase = Supabase.instance.client;


class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyApp> {

  final connectivity = Connectivity();
  late StreamSubscription<List<ConnectivityResult>> streamSubscription;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    streamSubscription = connectivity.onConnectivityChanged.listen((event) {
      if (event == ConnectivityResult.none){
        showError(context, "Network crashed");
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    streamSubscription.cancel();
  }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: light,
      home: SignUp(),
    );
  }
}
