
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class Item_Tile extends StatelessWidget{
  final String title;
  final Uint8List? icon;
  final String subtitle;
  final Function() ontap;
  final String? count_messages;

  Item_Tile({super.key, required this.title, required this.icon, required this.subtitle, required this.ontap, this.count_messages});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 84.2,
      decoration: BoxDecoration(
          color: Colors.white,
      borderRadius: BorderRadius.circular(10),
        border: Border.all(color: Color(0xFFA7A7A7), width: 1)
      ),
      child: ListTile(
        title: Text(title),
        contentPadding: EdgeInsets.symmetric(vertical: 5, horizontal: 8),
        subtitle: Text(subtitle),
        leading: (icon != null)?Container(height: 60.14, width: 60.14,
          child: Image.memory(icon!, fit: BoxFit.cover,),):Image.asset('assets/Frame 83.png', height: 60.14, width: 60.14,),
        trailing: (count_messages != 'null' && count_messages != '0')?ClipOval(
          child: Container(
            height: 25,
            width: 26,
            decoration: BoxDecoration(
              color: Color(0xFF0560FA),

            ),
            child: Text(count_messages!,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontSize: 12,
                fontFamily: 'Roboto',
                fontWeight: FontWeight.w400,
                height: 25/12
              ),),
          ),
        ):null,
        onTap: ontap,
      ),

    );
  }
}
