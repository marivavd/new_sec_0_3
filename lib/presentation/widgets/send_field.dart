
import 'package:flutter/material.dart';

class SendField extends StatelessWidget{
  final String hint;
  final TextEditingController controller;

  const SendField({super.key, required this.hint, required this.controller});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 32,
      decoration: BoxDecoration(
        color: Colors.white,
          boxShadow: [BoxShadow(
              color: Color(0x26000026),
              blurRadius: 5,
              offset: Offset(0, 2)
          )],
        border: Border.all(width: 0, color: Colors.white)
      ),
      child: TextField(
        controller: controller,
        style: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 12,
            height: 16/12,
            color: Color(0xFF3A3A3A)
        ),
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(vertical: 14, horizontal: 10),
          hintText: hint,
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent)
          ),
          hintStyle: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: 12,
            height: 16/12,
            color: Color(0xFFCFCFCF)
          ),
          border: OutlineInputBorder(
            borderSide: const BorderSide(
              width: 0,
              style: BorderStyle.none,
            ),
          ),
        ),

      ),
    );
  }

}