

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CustomField extends StatefulWidget{
  final String label;
  final String hint;
  final TextEditingController controller;
  final bool enableObscure;
  final bool isValid;
  final Function(String)? onChange;

  const CustomField({super.key, required this.label, required this.hint, required this.controller, this.enableObscure = false, this.isValid = true, this.onChange});

  @override
  State<CustomField> createState() => CustomFieldState();
}

class CustomFieldState extends State<CustomField>{
  bool isObscure = true;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.label,
          style: Theme.of(context).textTheme.titleMedium,
        ),
        SizedBox(height: 8,),
        SizedBox(
          height: 44,
          child: TextField(
            controller: widget.controller,
            onChanged: widget.onChange,
            obscuringCharacter: '*',
            obscureText: (widget.enableObscure)?isObscure:false,
            style: TextStyle(color: Color(0xFF3A3A3A), fontSize: 14, height: 16/14, fontWeight: FontWeight.w500),
            decoration: InputDecoration(
              hintText: widget.hint,
              hintStyle: Theme.of(context).textTheme.titleMedium?.copyWith(color: Color(0xFFCFCFCF)),
              enabledBorder: (widget.isValid) ? Theme.of(context).inputDecorationTheme.enabledBorder : Theme.of(context).inputDecorationTheme.errorBorder,
              focusedBorder: (widget.isValid) ? Theme.of(context).inputDecorationTheme.focusedBorder : Theme.of(context).inputDecorationTheme.errorBorder,
              contentPadding: EdgeInsets.symmetric(vertical: 14, horizontal: 10),
              suffixIconConstraints: BoxConstraints(minWidth: 34 ),
              suffixIcon: (widget.enableObscure)?
                  GestureDetector(
                    onTap: (){
                      setState(() {
                        isObscure = !isObscure;
                      });
                    },
                    child: SvgPicture.asset(
                        (isObscure)?"assets/eye-slash.svg":"assets/eye.svg", color: Color(0xFF141414), width: 14, height: 14,
                    ),
                  ):null
            ),
          ),
        )

      ],
    );
  }
}
