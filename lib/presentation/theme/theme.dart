import 'package:flutter/material.dart';

var light = ThemeData(
  textTheme: TextTheme(
    titleLarge: TextStyle(
      color: Color(0xFF3A3A3A),
      fontSize: 24,
      fontWeight: FontWeight.w500,
      height: 30/24
    ),
    titleMedium: TextStyle(
        color: Color(0xFFA7A7A7),
        fontSize: 14,
        fontWeight: FontWeight.w500,
        height: 16/14
    ),
    labelLarge: TextStyle(
        color: Colors.white,
        fontSize: 16,
        fontWeight: FontWeight.w700,
    ),
  ),
  filledButtonTheme: FilledButtonThemeData(
    style: FilledButton.styleFrom(
      backgroundColor: Color(0xFF0560FA),
      disabledBackgroundColor: Color(0xFFA7A7A7),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4)
      )
    )
  ),
  inputDecorationTheme: InputDecorationTheme(
    enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Color(0xFFA7A7A7))
    ),
    focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Color(0xFFA7A7A7))
    ),
    errorBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Color(0xFFED3A3A))
    ),
  ),

);