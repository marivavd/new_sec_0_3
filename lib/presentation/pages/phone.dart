import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:new_sec_0_3/data/models/modelprofile.dart';
import 'package:new_sec_0_3/domain/geolocator_use_case.dart';
import 'package:new_sec_0_3/domain/make_profile.dart';
import 'package:new_sec_0_3/presentation/dialog.dart';
import 'package:new_sec_0_3/presentation/pages/home.dart';
import 'package:new_sec_0_3/presentation/pages/profile.dart';
import 'package:new_sec_0_3/presentation/pages/send_package.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class Phone extends StatefulWidget {
  Phone({super.key, required this.profile});
  ModelProfile profile;

  @override
  State<Phone> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Phone> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    double widthScreen = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(

        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Spacer(),
         SizedBox(
             height:84, width: 84,
           child:  ClipOval(
             child: (widget.profile.avatar != null)?Container(height:84, width: 84,
               child: Image.memory(widget.profile.avatar, fit: BoxFit.cover,),):Image.asset('assets/Frame 83.png', height: 84, width: 84,),
           ),
         ),
          SizedBox(height: 9.33,),
          Text(
            widget.profile.fullname,
            style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: 18.67,
                color: Color(0xFF0560FA),
                height: 19/18.67,
                fontWeight: FontWeight.w700
            ),
          ),
          SizedBox(height: 9.33,),
          Text(
            widget.profile.phone,
            style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: 18.67,
                color: Color(0xFFA7A7A7),
                height: 19/18.67,
                fontWeight: FontWeight.w700
            ),
          ),
          SizedBox(height: 9.33,),
          Text(
            'calling...',
            style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: 14,
                color: Color(0xFF0560FA),
                height: 19/14,
                fontWeight: FontWeight.w400
            ),
          ),
          Spacer(),
          Container(
            height: 332.16,
            width: widthScreen - 48,
            decoration: BoxDecoration(
              color: Color(0xFFF2F2F2),
              borderRadius: BorderRadius.circular(8.12),

            ),
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 49.14, horizontal: 55.16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SvgPicture.asset('assets/Frame 161.svg', color: Color(0xFF3A3A3A)),
                      SvgPicture.asset('assets/Frame 160.svg', color: Color(0xFF3A3A3A)),
                      SvgPicture.asset('assets/Frame 159.svg', color: Color(0xFF3A3A3A)),
                    ],
                  ),
                  Spacer(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SvgPicture.asset('assets/Vector.svg', color: Color(0xFF3A3A3A)),
                      SvgPicture.asset('assets/Group.svg', color: Color(0xFF3A3A3A)),
                      SvgPicture.asset('assets/ion_keypad-outline.svg', color: Color(0xFF3A3A3A)),
                    ],
                  ),
                  Spacer(),
                  InkWell(
                    child: Image.asset('assets/Frame 156.png'),
                    onTap: (){
                      Navigator.of(context).pop();
                    },
                  )
                ],
              ),
            )
          ),
          Spacer(),

        ],
      ),)

    );
  }
}
