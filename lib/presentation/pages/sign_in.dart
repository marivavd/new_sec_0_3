import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:new_sec_0_3/domain/controllers/pass_controller.dart';
import 'package:new_sec_0_3/domain/sign_in_use_case.dart';
import 'package:new_sec_0_3/domain/sign_up_use_case.dart';
import 'package:new_sec_0_3/domain/utils.dart';
import 'package:new_sec_0_3/presentation/dialog.dart';
import 'package:new_sec_0_3/presentation/pages/forgot.dart';
import 'package:new_sec_0_3/presentation/pages/holder.dart';
import 'package:new_sec_0_3/presentation/pages/sign_up.dart';
import 'package:new_sec_0_3/presentation/widgets/custom_filed.dart';

class SignIn extends StatefulWidget {
  const SignIn({super.key});

  @override
  State<SignIn> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<SignIn> {
  var email = TextEditingController();
  var pass = PasswordController();
  bool check = false;
  bool isRedEmail = false;
  bool isValid = false;
  SignInUseCase useCase = SignInUseCase();


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      isRedEmail = !checkEmail(email.text) && email.text.isNotEmpty;
      isValid = !isRedEmail && pass.text.isNotEmpty;
    });

  }

  void onChange(_){
    setState(() {
      isRedEmail = !checkEmail(email.text) && email.text.isNotEmpty;
      isValid = !isRedEmail && pass.text.isNotEmpty;
    });
  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height:155,),
              Text(
                'Welcome Back',
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                "Fill in your email and password to continue",
                style: Theme.of(context).textTheme.titleMedium,
              ),
              SizedBox(height: 20,),
              CustomField(label: 'Email Address', hint: '***********@mail.com', controller: email, onChange: onChange, isValid: !isRedEmail,),
              SizedBox(height: 24,),
              CustomField(label: 'Password', hint: '**********', controller: pass, onChange: onChange, enableObscure: true,),
              SizedBox(height: 17,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      SizedBox(width: 1,),
                      SizedBox(
                        height: 14,
                        width: 14,
                        child: Checkbox(
                          value: check,
                          onChanged: (val){
                            setState(() {
                              check = val!;
                            });
                          },
                          side: BorderSide(
                              color: Color(0xFFA7A7A7),
                              width: 1
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(2)
                          ),
                          activeColor: Color(0xFF006CEC),
                        ),
                      ),
                      SizedBox(width: 4,),
                      Text(
                        'Remember password',
                        style: TextStyle(
                          fontSize: 12,
                          height: 16/12,
                          fontWeight: FontWeight.w500,
                          color: Color(0xFFA7A7A7),
                        ),
                      )
                    ],
                  ),
                  InkWell(
                    child: Text(
                      'Forgot Password?',
                      style: TextStyle(
                        fontSize: 12,
                        height: 16/12,
                        fontWeight: FontWeight.w500,
                        color: Color(0xFF0560FA),
                      ),
                    ),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => ForgotPass())).then((value) => setState(() {

                      }));
                    },
                  )
                ],
              ),
              SizedBox(height: 187,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    style: Theme.of(context).filledButtonTheme.style,
                    onPressed: (isValid)?()async{
                      showLoading(context);
                      useCase.pressbutton(
                          email.text,
                          pass.hash_pass(),
                              (_){
                            hideLoading(context);
                            Navigator.push(context, MaterialPageRoute(builder: (context) => Holder(current_index: 0))).then((value) => setState(() {

                            }));
                          },
                              (String error)async{
                            hideLoading(context);
                            showError(context, error);
                          });
                    }:null,
                    child: Text(
                      'Log in',
                      style: Theme.of(context).textTheme.labelLarge,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Already have an account?',
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
                  ),
                  InkWell(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => SignUp())).then((value) => setState(() {

                      }));

                    },
                    child: Text(
                      'Sign Up',
                      style: Theme.of(context).textTheme.titleMedium?.copyWith(color: Color(0xFF0560FA)),
                    ),
                  )
                ],
              ),
              SizedBox(height: 18,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [Column(
                  children: [
                    Text(
                      'or log in using',
                      style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
                    ),
                    SizedBox(height: 8,),
                    SvgPicture.asset('assets/google.svg')

                  ],
                )],
              ),
              SizedBox(height: 28,)

            ],
          ),
        ),
      ),
    );
  }
}
