import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:new_sec_0_3/domain/geolocator_use_case.dart';
import 'package:new_sec_0_3/presentation/dialog.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class MapPage extends StatefulWidget {
  const MapPage({super.key});

  @override
  State<MapPage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MapPage> {
  GeoUseCase useCase = GeoUseCase();
  String address = '';
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    useCase.getCurrentLocation(
        onResponse: (Position position){
          useCase.getCurrentAddress(
             Point(latitude: position.latitude, longitude: position.longitude),
              onResponse: (String ad){
               address = ad;
               setState(() {

               });
              },
              onError: (String e){
                showError(context, e);
              });
        },
        onError: (String e){
          showError(context, e);
        });
  }


  @override
  Widget build(BuildContext context) {

    return (address != '')?Scaffold(
      body: Center(
        child: Text(
          address
        ),
      ),
    ):Center(child: CircularProgressIndicator(),);
  }
}
