import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:new_sec_0_3/domain/controllers/pass_controller.dart';
import 'package:new_sec_0_3/domain/send_email_use_case.dart';
import 'package:new_sec_0_3/domain/sign_in_use_case.dart';
import 'package:new_sec_0_3/domain/sign_up_use_case.dart';
import 'package:new_sec_0_3/domain/utils.dart';
import 'package:new_sec_0_3/presentation/dialog.dart';
import 'package:new_sec_0_3/presentation/pages/holder.dart';
import 'package:new_sec_0_3/presentation/pages/otp.dart';
import 'package:new_sec_0_3/presentation/pages/sign_in.dart';
import 'package:new_sec_0_3/presentation/pages/sign_up.dart';
import 'package:new_sec_0_3/presentation/widgets/custom_filed.dart';

class ForgotPass extends StatefulWidget {
  const ForgotPass({super.key});

  @override
  State<ForgotPass> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<ForgotPass> {
  var email = TextEditingController();
  bool isRedEmail = false;
  bool isValid = false;
  SendEmailUseCase useCase = SendEmailUseCase();


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      isRedEmail = !checkEmail(email.text) && email.text.isNotEmpty;
      isValid = !isRedEmail;
    });

  }

  void onChange(_){
    setState(() {
      isRedEmail = !checkEmail(email.text) && email.text.isNotEmpty;
      isValid = !isRedEmail;
    });
  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height:155,),
              Text(
                'OTP',
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                "Enter your email address",
                style: Theme.of(context).textTheme.titleMedium,
              ),
              SizedBox(height: 56,),
              CustomField(label: 'Email Address', hint: '***********@mail.com', controller: email, onChange: onChange, isValid: !isRedEmail,),
              SizedBox(height: 56,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    style: Theme.of(context).filledButtonTheme.style,
                    onPressed: (isValid)?()async{
                      showLoading(context);
                      useCase.pressbutton(
                          email.text,
                              (_){
                            hideLoading(context);
                            Navigator.push(context, MaterialPageRoute(builder: (context) => OTP(email: email.text))).then((value) => setState(() {

                            }));
                          },
                              (String error)async{
                            hideLoading(context);
                            showError(context, error);
                          });
                    }:null,
                    child: Text(
                      'Send OTP',
                      style: Theme.of(context).textTheme.labelLarge,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Remember password? Back to ',
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
                  ),
                  InkWell(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn())).then((value) => setState(() {

                      }));

                    },
                    child: Text(
                      'Sign in',
                      style: Theme.of(context).textTheme.titleMedium?.copyWith(color: Color(0xFF0560FA)),
                    ),
                  )
                ],
              ),

              SizedBox(height: 28,)

            ],
          ),
        ),
      ),
    );
  }
}
