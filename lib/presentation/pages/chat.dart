import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:new_sec_0_3/data/models/modelmessage.dart';
import 'package:new_sec_0_3/data/models/modelprofile.dart';
import 'package:new_sec_0_3/data/repository/requests.dart';
import 'package:new_sec_0_3/domain/create_message.dart';
import 'package:new_sec_0_3/domain/geolocator_use_case.dart';
import 'package:new_sec_0_3/domain/get_messages.dart';
import 'package:new_sec_0_3/domain/make_profile.dart';
import 'package:new_sec_0_3/presentation/dialog.dart';
import 'package:new_sec_0_3/presentation/pages/chats.dart';
import 'package:new_sec_0_3/presentation/pages/home.dart';
import 'package:new_sec_0_3/presentation/pages/phone.dart';
import 'package:new_sec_0_3/presentation/pages/profile.dart';
import 'package:new_sec_0_3/presentation/pages/send_package.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class Chat extends StatefulWidget {
  Chat({super.key, required this.profile});
  ModelProfile profile;

  @override
  State<Chat> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Chat> {
  List<ModelMessage> messages = [];
  bool flag = false;
  var controller = TextEditingController();
  String idChat = '';


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      final messagesOld = await getMessages(widget.profile.id);
      final idChatOld = await getIdChat(widget.profile.id);
      setState(() {
        messages = messagesOld;
        idChat = idChatOld;
        flag = true;
      });

    });
    subsribeMessage((modelMessage) => {
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
        showLoading(context);
        final messagesOld = await getMessages(widget.profile.id);
        setState(() {
          messages = messagesOld;
          hideLoading(context);
        });

      })
    });


  }

  @override
  Widget build(BuildContext context) {

    return (flag)?Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
         Container(
          height: 108,
          width: double.infinity,
          alignment: Alignment.bottomLeft,
          padding: const EdgeInsets.only(left: 15, right: 15, bottom: 15, top:69),
          decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    color: Color(0xFF26000026),
                    blurRadius: 5,
                    offset: Offset(0, 2)
                )
              ]
          ),
          child: Row(
            children: [
              InkWell(
                child: SvgPicture.asset('assets/arrow-square-right.svg'),
                onTap: ()async{
                  showLoading(context);
                  final myUser = await makeProfile();
                  hideLoading(context);
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Chats(profile: myUser))).then((value) => setState(() {

                  }));
                },
              ),
              Expanded(child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ClipOval(
                    child: (widget.profile.avatar != null)?Container(
                      height: 43.08,
                      width: 43.08,
                      child: Image.memory(widget.profile.avatar, fit: BoxFit.cover,),

                    ): Image.asset("assets/Frame 83.png", height: 43.08, width: 43.08,),
                  ),
                  SizedBox(width: 8.62,),
                  SizedBox(
                    height: 28.87,
                    child: Text(

                      widget.profile.fullname,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 14,
                          color: Color(0xFF3A3A3A),
                          height: 16/14,
                          fontWeight: FontWeight.w400
                      ),
                    ),
                  )



                ],
              )),
              InkWell(
                child: SvgPicture.asset('assets/phone.svg'),
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Phone(profile: widget.profile)));
                },
              ),


            ],
          )
      ),
          SizedBox(height: 18,),

         Expanded(child:  ListView.builder(
           itemCount: messages.length,
           itemBuilder: (_, index){
             return Padding(
               padding: EdgeInsets.symmetric(horizontal: 24),
               child: Column(
                   crossAxisAlignment: (messages[index].idUser != widget.profile.id)?CrossAxisAlignment.end:CrossAxisAlignment.start,
                   children: [
                     SizedBox(height: 12,),
                     Container(
                         decoration: BoxDecoration(
                             color: (messages[index].idUser != widget.profile.id)?Color(0xFF0560FA):Color(0xFFCFCFCF),
                             borderRadius: BorderRadius.circular(8)
                         ),
                         child: Padding(
                           padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                           child: Text(messages[index].message,
                             style: TextStyle(
                                 fontFamily: 'Roboto',
                                 fontSize: 12,
                                 color: (messages[index].idUser != widget.profile.id)?Colors.white:Color(0xFF3A3A3A),
                                 height: 32/24,
                                 fontWeight: FontWeight.w500
                             ),
                           ),
                         )
                     ),
                   ]
               ),);
           },
         ),), Padding(
                padding: EdgeInsets.symmetric(horizontal: 24),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SvgPicture.asset('assets/Emoji.svg', color: Color(0xFFA7A7A7)),
                    Container(
                      height: 40,
                      width: 267,
                      decoration: BoxDecoration(
                        color: Color(0xFFCFCFCF),
                        borderRadius: BorderRadius.circular(8)
                      ),
                      child: TextField(

                        controller: controller,
                        decoration: InputDecoration(
                          focusColor: Colors.white,
                          hintText: 'Enter message',
                          contentPadding: EdgeInsets.symmetric(horizontal: 32),
                          hintStyle: TextStyle(
                              fontFamily: 'Roboto',
                              fontSize: 14,
                              color: Color(0xFFA7A7A7),
                              height: 16/14,
                              fontWeight: FontWeight.w400
                          ),
                            suffixIconConstraints: BoxConstraints(minWidth: 34 ),
                          suffixIcon: SvgPicture.asset('assets/Dictation.svg', color: Color(0xFFA7A7A7))

                        ),
                      ),
                    ),
                    InkWell(
                      child: SvgPicture.asset('assets/triangle.svg'),
                      onTap: ()async{
                        showLoading(context);
                        make_message(controller.text, idChat);

                        controller.clear();
                        setState(() {

                        });
                        hideLoading(context);
                      },
                    )
                  ],
                ),)




        ],
      ),
    ):Center(child: CircularProgressIndicator(),);
  }
}
