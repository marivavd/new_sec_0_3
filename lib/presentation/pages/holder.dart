import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:new_sec_0_3/data/models/modelmessage.dart';
import 'package:new_sec_0_3/data/models/modelprofile.dart';
import 'package:new_sec_0_3/data/repository/requests.dart';
import 'package:new_sec_0_3/domain/geolocator_use_case.dart';
import 'package:new_sec_0_3/domain/get_messages.dart';
import 'package:new_sec_0_3/domain/get_push_messages.dart';
import 'package:new_sec_0_3/domain/make_profile.dart';
import 'package:new_sec_0_3/domain/push_notification_use_case.dart';
import 'package:new_sec_0_3/presentation/dialog.dart';
import 'package:new_sec_0_3/presentation/pages/home.dart';
import 'package:new_sec_0_3/presentation/pages/profile.dart';
import 'package:new_sec_0_3/presentation/pages/send_package.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class Holder extends StatefulWidget {
  Holder({super.key, required this.current_index});
  int current_index;

  @override
  State<Holder> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Holder> {
  int index = 0;
  ModelProfile? profile;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    index = widget.current_index;
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
      profile = await makeProfile();

      setState(() {

      });
    });

  }


  @override
  Widget build(BuildContext context) {

    return (profile != null)?Scaffold(
      backgroundColor: Colors.white,
      body: [
        Home(profile: profile!,),
        Scaffold(backgroundColor: Colors.white,),
        Scaffold(backgroundColor: Colors.white,),
        Profile(profile: profile!)
      ][index],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: index,
        onTap: (val){
          setState(() {
            index = val;
          });
        },
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(icon: SvgPicture.asset("assets/house-2.svg", color: (index == 0)?Color(0xFF0560FA):Color(0xFFA7A7A7),), label: 'Home'),
          BottomNavigationBarItem(icon: SvgPicture.asset("assets/wallet-3.svg", color: (index == 1)?Color(0xFF0560FA):Color(0xFFA7A7A7),), label: 'Wallet'),
          BottomNavigationBarItem(icon: SvgPicture.asset("assets/smart-car.svg", color: (index == 2)?Color(0xFF0560FA):Color(0xFFA7A7A7),), label: 'Track'),
          BottomNavigationBarItem(icon: SvgPicture.asset("assets/profile-circle.svg", color: (index == 3)?Color(0xFF0560FA):Color(0xFFA7A7A7),), label: 'Profile'),
        ],
      ),
    ):Center(child: CircularProgressIndicator(),);
  }
}
