import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:new_sec_0_3/domain/controllers/pass_controller.dart';
import 'package:new_sec_0_3/domain/sign_up_use_case.dart';
import 'package:new_sec_0_3/domain/utils.dart';
import 'package:new_sec_0_3/presentation/dialog.dart';
import 'package:new_sec_0_3/presentation/pages/sign_in.dart';
import 'package:new_sec_0_3/presentation/widgets/custom_filed.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});

  @override
  State<SignUp> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<SignUp> {
  var fullname = TextEditingController();
  var phone = MaskedTextController(mask: '+0(000)000-00-00');
  var email = TextEditingController();
  var pass = PasswordController();
  var confPass = PasswordController();
  bool check = false;
  bool isRedEmail = false;
  bool isValid = false;
  SignUpUseCase useCase = SignUpUseCase();


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      isRedEmail = !checkEmail(email.text) && email.text.isNotEmpty;
      isValid = !isRedEmail && fullname.text.isNotEmpty && phone.text.isNotEmpty && pass.text.isNotEmpty && confPass.text.isNotEmpty && check;
    });

  }

  void onChange(_){
    setState(() {
      isRedEmail = !checkEmail(email.text) && email.text.isNotEmpty;
      isValid = !isRedEmail && fullname.text.isNotEmpty && phone.text.isNotEmpty && pass.text.isNotEmpty && confPass.text.isNotEmpty && check;
    });
  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 78,),
              Text(
                'Create an account',
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                "Complete the sign up process to get started",
                style: Theme.of(context).textTheme.titleMedium,
              ),
              SizedBox(height: 33,),
              CustomField(label: 'Full name', hint: 'Ivanov Ivan', controller: fullname, onChange: onChange,),
              SizedBox(height: 24,),
              CustomField(label: 'Phone Number', hint: '+7(999)999-99-99', controller: phone, onChange: onChange,),
              SizedBox(height: 24,),
              CustomField(label: 'Email Address', hint: '***********@mail.com', controller: email, onChange: onChange, isValid: !isRedEmail,),
              SizedBox(height: 24,),
              CustomField(label: 'Password', hint: '**********', controller: pass, onChange: onChange, enableObscure: true,),
              SizedBox(height: 24,),
              CustomField(label: 'Confirm Password', hint: '**********', controller: confPass, onChange: onChange, enableObscure: true,),
              SizedBox(height: 37,),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(width: 1,),
                  SizedBox(
                    height: 14,
                    width: 14,
                    child: Checkbox(
                      value: check,
                      onChanged: (val)async{
                        setState(() {
                          check = val!;
                          onChange(check);
                        });
                        setState(() {

                        });

                      },
                      side: BorderSide(
                        color: Color(0xFF006CEC),
                        width: 1
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(2)
                      ),
                      activeColor: Color(0xFF006CEC),
                    ),
                  ),
                  SizedBox(width: 11,),
                  RichText(
                    textAlign: TextAlign.center,
                      text: TextSpan(
                        text: "By ticking this box, you agree to our ",
                        style: TextStyle(
                            color: Color(0xFFA7A7A7),
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                          height: 28/24
                        ),
                        children: [
                          TextSpan(
                            text: "Terms and \nconditions and private policy",
                            style: TextStyle(
                                color: Color(0xFFEBBC2E),
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                height: 28/24
                            ),
                          )
                        ]
                      ),
                  )
                ],
              ),
              SizedBox(height: 64,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    style: Theme.of(context).filledButtonTheme.style,
                    onPressed: (isValid)?()async{
                      showLoading(context);
                      useCase.pressbutton(
                          fullname.text,
                          phone.text,
                          email.text,
                          pass.hash_pass(),
                          confPass.hash_pass(),
                          (_){
                            hideLoading(context);
                            Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn())).then((value) => setState(() {

                            }));
                          },
                          (String error)async{
                            hideLoading(context);
                            showError(context, error);
                          });
                    }:null,
                    child: Text(
                      'Sign Up',
                      style: Theme.of(context).textTheme.labelLarge,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Already have an account?',
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
                  ),
                  InkWell(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn())).then((value) => setState(() {

                      }));

                    },
                    child: Text(
                      'Sign in',
                      style: Theme.of(context).textTheme.titleMedium?.copyWith(color: Color(0xFF0560FA)),
                    ),
                  )
                ],
              ),
              SizedBox(height: 18,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [Column(
                  children: [
                    Text(
                      'or sign in using',
                      style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400),
                    ),
                    SizedBox(height: 8,),
                    SvgPicture.asset('assets/google.svg')

                  ],
                )],
              ),
              SizedBox(height: 28,)

            ],
          ),
        ),
      ),
    );
  }
}
