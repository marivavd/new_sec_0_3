import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:new_sec_0_3/data/models/modelorder.dart';
import 'package:new_sec_0_3/data/models/modelprofile.dart';
import 'package:new_sec_0_3/domain/createdesttext.dart';
import 'package:new_sec_0_3/domain/geolocator_use_case.dart';
import 'package:new_sec_0_3/domain/log_out_use_case.dart';
import 'package:new_sec_0_3/domain/make_order.dart';
import 'package:new_sec_0_3/presentation/dialog.dart';
import 'package:new_sec_0_3/presentation/pages/sign_in.dart';
import 'package:new_sec_0_3/presentation/pages/transaction.dart';
import 'package:new_sec_0_3/presentation/widgets/send_field.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class Sendpackage2 extends StatefulWidget {
  Sendpackage2({super.key, required this.order});
  ModelOrder order;

  @override
  State<Sendpackage2> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Sendpackage2> {
  String destinationText = '';


  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
      destinationText = await createDestText(widget.order.id);
      setState(() {

      });
    });

  }



  @override
  Widget build(BuildContext context) {

    return (destinationText != '')?Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 108,
            width: double.infinity,
            padding: EdgeInsets.only(left: 14, right: 14, bottom: 19),
            decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [BoxShadow(
                    color: Color(0x26000026),
                    blurRadius: 5,
                    offset: Offset(0, 2)
                )]
            ),
            child: Stack(
              children: [
                Align(
                    alignment: Alignment.bottomLeft,
                    child: InkWell(
                      onTap: (){
                        Navigator.of(context).pop();
                      },
                      child: SvgPicture.asset('assets/arrow-square-right.svg'),
                    )
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Text(
                    'Send a package',
                    style: TextStyle(
                        color: Color(0xFFA7A7A7),
                        fontWeight: FontWeight.w500,
                        fontSize: 16
                    ),
                  ),
                )
              ],
            ),
          ),
          Text(widget.order.id),
          Text("Destination details"),
          Text(destinationText),
          OutlinedButton(onPressed: (){Navigator.of(context).pop();},
              child: Text('Edit package'),
          ),
          OutlinedButton(onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context) => Transaction(id: widget.order.id))).then((value) => setState(() {

            }));
          }, child: Text('Make payment'))
        ],
      ),
    ):Center(child: CircularProgressIndicator(),);
  }
}
