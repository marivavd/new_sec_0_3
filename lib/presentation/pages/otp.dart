import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:new_sec_0_3/data/repository/requests.dart';
import 'package:new_sec_0_3/domain/controllers/pass_controller.dart';
import 'package:new_sec_0_3/domain/otp_use_case.dart';
import 'package:new_sec_0_3/domain/send_email_use_case.dart';
import 'package:new_sec_0_3/domain/sign_in_use_case.dart';
import 'package:new_sec_0_3/domain/sign_up_use_case.dart';
import 'package:new_sec_0_3/domain/utils.dart';
import 'package:new_sec_0_3/presentation/dialog.dart';
import 'package:new_sec_0_3/presentation/pages/change_pass.dart';
import 'package:new_sec_0_3/presentation/pages/holder.dart';
import 'package:new_sec_0_3/presentation/pages/sign_in.dart';
import 'package:new_sec_0_3/presentation/pages/sign_up.dart';
import 'package:new_sec_0_3/presentation/widgets/custom_filed.dart';

class OTP extends StatefulWidget {
  OTP({super.key, required this.email});
  String email;

  @override
  State<OTP> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<OTP> {
  var code = TextEditingController();
  bool isValid = false;
  OtPUseCase useCase = OtPUseCase();
  SendEmailUseCase sendUsecase = SendEmailUseCase();


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      isValid = (code.text.length == 6);
    });

  }

  void onChange(_){
    setState(() {
      isValid = (code.text.length == 6);
    });
  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height:155,),
              Text(
                'OTP Verification',
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                "Enter the 6 digit numbers sent to your email",
                style: Theme.of(context).textTheme.titleMedium,
              ),
              SizedBox(height: 55,),
              CustomField(label: '', hint: '', controller: code, onChange: onChange),
              SizedBox(height: 12,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'If you didn’t receive code, ',
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400)
                  ),
                  InkWell(
                    onTap: ()async{
                      showLoading(context);
                      sendUsecase.pressbutton(
                          widget.email,
                              (_){
                            hideLoading(context);
                            Navigator.push(context, MaterialPageRoute(builder: (context) => OTP(email: widget.email))).then((value) => setState(() {

                            }));
                          },
                              (String error)async{
                            hideLoading(context);
                            showError(context, error);
                          });
                    },
                    child: Text(
                      "resend",
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400, color: Color(0xFF0560FA))
                    ),
                  )
                ],
              ),
              SizedBox(height: 84,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    style: Theme.of(context).filledButtonTheme.style,
                    onPressed: (isValid)?()async{
                      showLoading(context);
                      useCase.pressbutton(
                          widget.email,
                              code.text,
                              (_){
                            hideLoading(context);
                            Navigator.push(context, MaterialPageRoute(builder: (context) => ChangePass())).then((value) => setState(() {

                            }));
                          },
                              (String error)async{
                            hideLoading(context);
                            showError(context, error);
                          });
                    }:null,
                    child: Text(
                      'Set New Password',
                      style: Theme.of(context).textTheme.labelLarge,
                    ),
                  ),
                ),
              ),


              SizedBox(height: 28,)

            ],
          ),
        ),
      ),
    );
  }
}
