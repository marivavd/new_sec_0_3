import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:new_sec_0_3/domain/change_pass_use_case.dart';
import 'package:new_sec_0_3/domain/controllers/pass_controller.dart';
import 'package:new_sec_0_3/domain/sign_in_use_case.dart';
import 'package:new_sec_0_3/domain/sign_up_use_case.dart';
import 'package:new_sec_0_3/domain/utils.dart';
import 'package:new_sec_0_3/presentation/dialog.dart';
import 'package:new_sec_0_3/presentation/pages/forgot.dart';
import 'package:new_sec_0_3/presentation/pages/holder.dart';
import 'package:new_sec_0_3/presentation/pages/sign_up.dart';
import 'package:new_sec_0_3/presentation/widgets/custom_filed.dart';

class ChangePass extends StatefulWidget {
  const ChangePass({super.key});

  @override
  State<ChangePass> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<ChangePass> {
  var pass = PasswordController();
  var confPass = PasswordController();
  bool isValid = false;
  ChangePassUseCase useCase = ChangePassUseCase();


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      isValid = confPass.text.isNotEmpty && pass.text.isNotEmpty;
    });

  }

  void onChange(_){
    setState(() {
      isValid = confPass.text.isNotEmpty && pass.text.isNotEmpty;
    });
  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height:155,),
              Text(
                'New Password',
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                "Enter new password",
                style: Theme.of(context).textTheme.titleMedium,
              ),
              SizedBox(height: 70,),
              CustomField(label: 'Password', hint: '**********', controller: pass, onChange: onChange, enableObscure: true,),
              SizedBox(height: 24,),
              CustomField(label: 'Confirm Password', hint: '**********', controller: confPass, onChange: onChange, enableObscure: true,),
              SizedBox(height: 53,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    style: Theme.of(context).filledButtonTheme.style,
                    onPressed: (isValid)?()async{
                      showLoading(context);
                      useCase.pressbutton(
                          pass.hash_pass(),
                          confPass.hash_pass(),
                              (_){
                            hideLoading(context);
                            Navigator.push(context, MaterialPageRoute(builder: (context) => Holder(current_index: 0))).then((value) => setState(() {

                            }));
                          },
                              (String error)async{
                            hideLoading(context);
                            showError(context, error);
                          });
                    }:null,
                    child: Text(
                      'Log in',
                      style: Theme.of(context).textTheme.labelLarge,
                    ),
                  ),
                ),
              ),

              SizedBox(height: 28,)

            ],
          ),
        ),
      ),
    );
  }
}
