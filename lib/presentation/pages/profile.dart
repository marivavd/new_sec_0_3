import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:new_sec_0_3/data/models/modelprofile.dart';
import 'package:new_sec_0_3/domain/geolocator_use_case.dart';
import 'package:new_sec_0_3/domain/log_out_use_case.dart';
import 'package:new_sec_0_3/presentation/dialog.dart';
import 'package:new_sec_0_3/presentation/pages/add_payment.dart';
import 'package:new_sec_0_3/presentation/pages/notification.dart';
import 'package:new_sec_0_3/presentation/pages/sign_in.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class Profile extends StatefulWidget {
  Profile({super.key, required this.profile});
  ModelProfile profile;

  @override
  State<Profile> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Profile> {
  bool isVisible = true;
  LogOutUseCase useCase = LogOutUseCase();


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 108,
              width: double.infinity,
              padding: EdgeInsets.only(left: 14, right: 14, bottom: 19),
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [BoxShadow(
                    color: Color(0x26000026),
                    blurRadius: 5,
                    offset: Offset(0, 2)
                )]
              ),
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Text(
                      'Profile',
                      style: TextStyle(
                        color: Color(0xFFA7A7A7),
                        fontWeight: FontWeight.w500,
                        fontSize: 16
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 27,),
            Padding(
                padding: EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Image.asset('assets/Frame 83.png'),
                          SizedBox(width: 10,),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Hello ${widget.profile.fullname}",
                                style: TextStyle(
                                  height: 30/16,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500,
                                    color: Color(0xFF3A3A3A)
                                ),
                              ),
                              Row(
                                children: [
                                  Text(
                                    'Current balance:',
                                    style: TextStyle(
                                        height: 16/12,
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        color: Color(0xFF3A3A3A)
                                    ),
                                  ),
                                  Text(
                                      (isVisible)?'N${widget.profile.balance}:00':'********',
                                    style: TextStyle(
                                        height: 16/12,
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500,
                                        color: Color(0xFF0560FA)
                                    ),
                                  )
                                ],
                              )
                            ],
                          )
                        ],
                      ),
                      InkWell(
                        onTap: (){
                          setState(() {
                            isVisible = !isVisible;
                          });
                        },
                        child: SvgPicture.asset('assets/eye-slash1.svg'),
                      )
                    ],
                  ),
                  OutlinedButton(onPressed: ()async{
                    showLoading(context);
                    useCase.pressbutton(
                            (_){
                          hideLoading(context);
                          Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn())).then((value) => setState(() {

                          }));
                        },
                            (String error)async{
                          hideLoading(context);
                          showError(context, error);
                        }
                    );
                  }, child: Text('Log out')),
                  OutlinedButton(
                      onPressed: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => Notif())).then((value) => setState(() {

                        }));
                      },
                      child: Text('Notification')),
                  OutlinedButton(
                      onPressed: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => AddPay())).then((value) => setState(() {

                        }));
                      },
                      child: Text('Add payment'))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
