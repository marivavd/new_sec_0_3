import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:new_sec_0_3/data/models/modelprofile.dart';
import 'package:new_sec_0_3/domain/geolocator_use_case.dart';
import 'package:new_sec_0_3/domain/log_out_use_case.dart';
import 'package:new_sec_0_3/domain/make_order.dart';
import 'package:new_sec_0_3/presentation/dialog.dart';
import 'package:new_sec_0_3/presentation/pages/send_package2.dart';
import 'package:new_sec_0_3/presentation/pages/sign_in.dart';
import 'package:new_sec_0_3/presentation/widgets/send_field.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class Sendpackage extends StatefulWidget {
  Sendpackage({super.key});

  @override
  State<Sendpackage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Sendpackage> {
  bool isVisible = true;
  var address = TextEditingController(text: '');
  var country = TextEditingController();
  var phone = TextEditingController();
  var others = TextEditingController();
  var packageItems = TextEditingController();
  var weight = TextEditingController();
  var worth = TextEditingController();
  List<TextEditingController> adresses = [TextEditingController()];
  List<TextEditingController> countries = [TextEditingController()];
  List<TextEditingController> phones = [TextEditingController()];
  List<TextEditingController> otherses = [TextEditingController()];
  GeoUseCase useCase = GeoUseCase();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    useCase.getCurrentLocation(
        onResponse: (Position position){
          useCase.getCurrentAddress(
              Point(latitude: position.latitude, longitude: position.longitude),
              onResponse: (String ad){
                address.text = ad;
                setState(() {

                });
              },
              onError: (String e){
                showError(context, e);
              });
        },
        onError: (String e){
          showError(context, e);
        });
  }



  @override
  Widget build(BuildContext context) {

    return (address.text != '')?Scaffold(
      backgroundColor: Colors.white,
      body: CustomScrollView(
        slivers: [SliverToBoxAdapter(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 108,
                  width: double.infinity,
                  padding: EdgeInsets.only(left: 14, right: 14, bottom: 19),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [BoxShadow(
                          color: Color(0x26000026),
                          blurRadius: 5,
                          offset: Offset(0, 2)
                      )]
                  ),
                  child: Stack(
                    children: [
                      Align(
                          alignment: Alignment.bottomLeft,
                          child: InkWell(
                            onTap: (){
                              Navigator.of(context).pop();
                            },
                            child: SvgPicture.asset('assets/arrow-square-right.svg'),
                          )
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Text(
                          'Send a package',
                          style: TextStyle(
                              color: Color(0xFFA7A7A7),
                              fontWeight: FontWeight.w500,
                              fontSize: 16
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 43,),]),
        ),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      SvgPicture.asset('assets/origin.svg'),
                      SizedBox(width: 8,),
                      Text(
                        'Origin Details',
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          height: 16/14,
                            color: Color(0xFF3A3A3A)
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 5,),
                  SendField(hint: 'Address', controller: address),
                  SizedBox(height: 5,),
                  SendField(hint: 'State,Country', controller: country),
                  SizedBox(height: 5,),
                  SendField(hint: 'Phone number', controller: phone),
                  SizedBox(height: 5,),
                  SendField(hint: 'Others', controller: others),
                ],
              ),
            ),
          ),
          SliverList.builder(
            itemCount: adresses.length,
              itemBuilder: (_, index){
                return Padding(
                    padding: EdgeInsets.symmetric(horizontal: 24),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 39,),
                      Row(
                        children: [
                          SvgPicture.asset('assets/destination.svg'),
                          SizedBox(width: 8,),
                          Text(
                            'Destination Details',
                            style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                                height: 16/14,
                                color: Color(0xFF3A3A3A)
                            ),
                          ),]
                      ),
                          SizedBox(height: 5,),
                          SendField(hint: 'Address', controller: adresses[index]),
                          SizedBox(height: 5,),
                          SendField(hint: 'State,Country', controller: countries[index]),
                          SizedBox(height: 5,),
                          SendField(hint: 'Phone number', controller: phones[index]),
                          SizedBox(height: 5,),
                          SendField(hint: 'Others', controller: otherses[index]),
                    ],
                  ),
                );
              }),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 10,),
                  Row(
                    children: [
                      InkWell(
                        onTap: (){
                          adresses.add(TextEditingController());
                          phones.add(TextEditingController());
                          countries.add(TextEditingController());
                          otherses.add(TextEditingController());
                          setState(() {

                          });
                        },
                        child: SvgPicture.asset('assets/add-square.svg'),
                      ),
                      SizedBox(width: 6,),
                      Text(
                        'Add destination',
                        style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 12,
                          height: 16/12,
                          color: Color(0xFFA7A7A7)
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 13,),
                      Text(
                        'Package Details',
                        style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            height: 16/14,
                            color: Color(0xFF3A3A3A)
                        ),
                      ),
                  SizedBox(height: 5,),
                  SendField(hint: 'package items', controller: packageItems),
                  SizedBox(height: 5,),
                  SendField(hint: 'Weight of item(kg)', controller: weight),
                  SizedBox(height: 5,),
                  SendField(hint: 'Worth of Items', controller: worth),
                  SizedBox(height: 39,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      OutlinedButton(
                          onPressed: ()async{
                            showLoading(context);
                            await makeOrder(
                                others: others.text,
                                phone: phone.text, country: country.text,
                                address: address.text,
                                items: packageItems.text,
                                weight: weight.text,
                                worth: worth.text,
                                adresses: adresses,
                                phones: phones,
                                countries: countries,
                                otherses: otherses,
                                onResponse: (order){
                                  hideLoading(context);
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => Sendpackage2(order: order))).then((value) => setState(() {

                                  }));
                                },
                                onError: (String e){
                                  hideLoading(context);
                                  showError(context, e);
                                });
                          },
                          child: Text(
                            'Instant delivery'
                          )),
                      OutlinedButton(
                          onPressed: (){},
                          child: Text(
                              'Scheduled delivery'
                          )),
                    ],
                  )
                ],
              ),
            ),
          )

      ]),
    ):Center(child: CircularProgressIndicator(),);
  }
}
