import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:new_sec_0_3/data/models/modelprofile.dart';
import 'package:new_sec_0_3/domain/geolocator_use_case.dart';
import 'package:new_sec_0_3/domain/log_out_use_case.dart';
import 'package:new_sec_0_3/presentation/dialog.dart';
import 'package:new_sec_0_3/presentation/pages/holder.dart';
import 'package:new_sec_0_3/presentation/pages/sign_in.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class Transaction extends StatefulWidget {
  Transaction({super.key, required this.id});
  String id;

  @override
  State<Transaction> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Transaction> with SingleTickerProviderStateMixin{
  late AnimationController animationController;
  bool finish = false;

  @override
  void initState() {
    // TODO: implement initState
    animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 5000));
    super.initState();


  }



  @override
  Widget build(BuildContext context) {
    animationController.forward().whenComplete(() => setState(() {
      finish = true;
    }));

    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 144,),
            (!finish)?RotationTransition(turns: Tween(begin: 1.0, end: 0.0).animate(animationController),
            child: Image.asset('assets/Component 21.png'),):Image.asset('assets/Good Tick.png'),
            SizedBox(height: 75,),
            (finish)?Text("Transaction Successful",
            style: TextStyle(
              fontSize: 24,
              height: 30/24,
              color: Color(0xFF3A3A3A),
              fontWeight: FontWeight.w500
            ),):SizedBox(height: 30,),
            SizedBox(height: 8,),
            Text(
              "Your rider is on the way to your destination",
              style:  TextStyle(
                  fontSize: 14,
                  height: 16/14,
                  color: Color(0xFF3A3A3A),
                  fontWeight: FontWeight.w400
              ),
            ),
            SizedBox(height: 8,),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Tracking Number ',
                  style:  TextStyle(
                      fontSize: 14,
                      height: 16/14,
                      color: Color(0xFF3A3A3A),
                      fontWeight: FontWeight.w400
                  ),
                ),
                Text(
                  widget.id,
                  style:  TextStyle(
                      fontSize: 14,
                      height: 16/14,
                      color: Color(0xFF0560FA),
                      fontWeight: FontWeight.w400
                  ),
                ),
              ],
            ),
            SizedBox(height: 100,),
            OutlinedButton(
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Holder(current_index: 2))).then((value) => setState(() {

                  }));
                }, child: Text(
              'Track my item'
            )),
            SizedBox(height: 34,),
            OutlinedButton(onPressed: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => Holder(current_index: 0))).then((value) => setState(() {

              }));
            },
                child: Text('Go back to homepage'))
          ],
        ),
      ),
    );
  }
}
