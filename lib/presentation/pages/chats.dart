import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:new_sec_0_3/data/models/modelchat.dart';
import 'package:new_sec_0_3/data/models/modelmessage.dart';
import 'package:new_sec_0_3/data/models/modelprofile.dart';
import 'package:new_sec_0_3/data/repository/requests.dart';
import 'package:new_sec_0_3/domain/avatar_use_case.dart';
import 'package:new_sec_0_3/domain/create_ads_url_list.dart';
import 'package:new_sec_0_3/domain/create_list_of_model_chats.dart';
import 'package:new_sec_0_3/domain/geolocator_use_case.dart';
import 'package:new_sec_0_3/domain/get_messages.dart';
import 'package:new_sec_0_3/domain/make_profile.dart';
import 'package:new_sec_0_3/domain/push_notification_use_case.dart';
import 'package:new_sec_0_3/domain/search_profiles.dart';
import 'package:new_sec_0_3/presentation/dialog.dart';
import 'package:new_sec_0_3/presentation/pages/chat.dart';
import 'package:new_sec_0_3/presentation/pages/home.dart';
import 'package:new_sec_0_3/presentation/pages/profile.dart';
import 'package:new_sec_0_3/presentation/pages/send_package.dart';
import 'package:new_sec_0_3/presentation/widgets/item_tile.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

import '../../domain/get_push_messages.dart';

class Chats extends StatefulWidget {
  Chats({super.key, required this.profile});
  ModelProfile profile;

  @override
  State<Chats> createState() => _MyHomePageState();
}




class _MyHomePageState extends State<Chats> {
  TextEditingController controller = TextEditingController();
  List<ModelChat> current_listofChats = [];
  Map<String, int> current_not_readen_messages = {};
  List<ModelProfile> listProfilewithrider = [];
  List<ModelProfile> listProfilewithriderSearch = [];
  bool flag = true;
  List<ModelMessage> allMessages = [];
  List<ModelMessage> pushMessages = [];
  late NotificationUseCase useCase;
  var isGrantPermission = false;
  var isLoading = true;

  void init() async {
    useCase = NotificationUseCase((message) => showMessageDialog(context, message));
    await useCase.init();
    var value = await useCase.requestPermission();
    setState(() {
      isGrantPermission = value;
      isLoading = false;
    });
  }


  void showMessageDialog(BuildContext context, String message){
    showDialog(context: context, builder: (_) => AlertDialog(title: Text(message)));
  }






  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    init();
    subsribeMessage((modelMessage) => {
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
        final allMessagesOld = await getAllMessagestoProfile();
        pushMessages = getPushMessages(allMessages, allMessagesOld);
        current_not_readen_messages = getNotReadenMessages(pushMessages, current_listofChats, current_not_readen_messages);
        for (var val in pushMessages){
          useCase.pushNotification(val);
        }
        allMessages = allMessagesOld;
        pushMessages.clear();
        final current_listofChatsOld = await createListofModelChat();
        current_listofChats = current_listofChatsOld;
        setState(() {

        });

      })
    });

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
      showLoading(context);
      final current_listofChatsOld = await createListofModelChat();
      for (var val in current_listofChatsOld){
        current_not_readen_messages[val.id] = 0;
      }
      final listProfilewithriderOld = await makeListProfiles();
      final allMessagesOld = await getAllMessagestoProfile();
      setState(() {
        current_listofChats = current_listofChatsOld;
        listProfilewithrider = listProfilewithriderOld;
        allMessages = allMessagesOld;
        listProfilewithriderSearch = listProfilewithrider;
        hideLoading(context);
      });

    });


  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.white,
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 108,
                  width: double.infinity,
                  padding: EdgeInsets.only(left: 14, right: 14, bottom: 19),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [BoxShadow(
                          color: Color(0x26000026),
                          blurRadius: 5,
                          offset: Offset(0, 2)
                      )]
                  ),
                  child: Stack(
                    children: [
                      Align(
                          alignment: Alignment.bottomLeft,
                          child: InkWell(
                            onTap: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context) => Home(profile: widget.profile))).then((value) => setState(() {

                              }));
                            },
                            child: SvgPicture.asset('assets/arrow-square-right.svg'),
                          )
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Text(
                          'Chats',
                          style: TextStyle(
                              color: Color(0xFFA7A7A7),
                              fontWeight: FontWeight.w500,
                              fontSize: 16
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 21,),
                  Align(
                    alignment: Alignment.center,
                    child: Container(
                      height: 40,
                      decoration: BoxDecoration(
                        color: Color(0xFFCFCFCF),
                        borderRadius: BorderRadius.circular(4)
                      ),
                      child: TextField(
                        controller: controller,
                        onChanged: (_){
                          if (controller.text == ''){
                            flag = true;
                          }
                          else{
                            flag = false;
                            listProfilewithriderSearch = searchProfiles(listProfilewithrider, controller.text);
                            print(controller.text);
                          }
                          setState(() {

                          });
                        },
                        decoration: InputDecoration(
                          hintText: 'Search for a driver',
                          hintStyle: TextStyle(
                            fontSize: 12,
                            height: 16/12,
                            color: Color(0xFFA7A7A7),
                            fontWeight: FontWeight.w400,
                            fontFamily: 'Roboto'
                          ),
                          //contentPadding: EdgeInsets.symmetric(horizontal: 12, vertical: 12),
                            suffixIconConstraints: BoxConstraints(minWidth: 34 ),
                          suffixIcon: SvgPicture.asset('assets/search-normal.svg')
                        ),
                      ),
                    )
                  ),
                  SizedBox(height: 27,)
                ],
              ),
            ),
          ),
          (flag)?
          SliverList.separated(
              separatorBuilder: (_, index){return SizedBox(height: 16,);},
              itemCount: current_listofChats.length,
              itemBuilder: (_, index){
                return Padding(padding: EdgeInsets.symmetric(horizontal: 24),
                child: Item_Tile(title: current_listofChats[index].profile.fullname, icon: current_listofChats[index].profile.avatar, subtitle: current_listofChats[index].lastMessage['message'],count_messages: current_not_readen_messages[current_listofChats[index].id].toString(),
                    ontap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Chat(profile: current_listofChats[index].profile))).then((value) => setState(() {

                  }));
                }),);
              })
              :
          SliverList.separated(
              separatorBuilder: (_, index){return SizedBox(height: 16,);},
              itemCount: listProfilewithriderSearch.length,
              itemBuilder: (_, index){
                return Padding(padding: EdgeInsets.symmetric(horizontal: 24),child: Item_Tile(title: listProfilewithriderSearch[index].fullname, icon: listProfilewithriderSearch[index].avatar, subtitle: '', count_messages: '0', ontap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Chat(profile:  listProfilewithriderSearch[index]))).then((value) => setState(() {

                  }));
                }),);
              })
      ]),
    );
  }
}
