import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:new_sec_0_3/data/models/modelprofile.dart';
import 'package:new_sec_0_3/domain/avatar_use_case.dart';
import 'package:new_sec_0_3/domain/create_ads_url_list.dart';
import 'package:new_sec_0_3/domain/geolocator_use_case.dart';
import 'package:new_sec_0_3/domain/make_profile.dart';
import 'package:new_sec_0_3/presentation/dialog.dart';
import 'package:new_sec_0_3/presentation/pages/chats.dart';
import 'package:new_sec_0_3/presentation/pages/home.dart';
import 'package:new_sec_0_3/presentation/pages/profile.dart';
import 'package:new_sec_0_3/presentation/pages/send_package.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class Home extends StatefulWidget {
  Home({super.key, required this.profile});
  ModelProfile profile;

  @override
  State<Home> createState() => _MyHomePageState();
}




class _MyHomePageState extends State<Home> {
  int focus = 0;
  int index = 0;
  List<String> adsUrl = [];
  ScrollController controller = ScrollController();
  AvatarUseCase? useCase;


  void onRemoveAvatar(){
    setState(() {
      widget.profile.avatar = null;
    });
  }

  void onPickAvatar(Uint8List bytes){
    setState(() {
      widget.profile.avatar = bytes;
    });
  }


  Future<ImageSource?> onChooseSource() async {

    ImageSource? imageSource;

    await showDialog(context: context, builder: (_) => AlertDialog(
      title: const Text("Выберите источник"),
      actions: [
        TextButton(onPressed: (){
          imageSource = ImageSource.camera;
          Navigator.pop(context);
        }, child: const Text("Камера")),
        TextButton(onPressed: (){
          imageSource = ImageSource.gallery;
          Navigator.pop(context);
        }, child: const Text("Галлерея"))
      ],
    ));

    return imageSource;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    useCase = AvatarUseCase(

      onChooseSource: onChooseSource,
      onPickAvatar: onPickAvatar,
      onRemoveAvatar: onRemoveAvatar,
    );
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{


      final adsUrlOld = await createListAdsUrl();
      setState(() {
        adsUrl = adsUrlOld;
      });
    });
  }



  @override
  Widget build(BuildContext context) {
    double widthScreen = MediaQuery.of(context).size.width;
    double separator = widthScreen - 48 - 318;


    return (adsUrl.length != 0)?Scaffold(
      backgroundColor: Colors.white,
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
       child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 69,),
              Align(
                alignment: Alignment.center,
                child: Image.asset('assets/img.png'),
              ),
              SizedBox(height: 35,),
              Container(
                height: 84,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Color(0xFF0560FA),
                  borderRadius: BorderRadius.circular(8),

                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        SizedBox(width: 10,),
                        InkWell(
                          child: ClipOval(
                            child: (widget.profile.avatar != null)?Container(height: 60.14, width: 60.14,
                              child: Image.memory(widget.profile.avatar, fit: BoxFit.cover,),):Image.asset('assets/Frame 83.png', height: 60.14, width: 60.14,),
                          ),
                          onTap: (){
                            useCase?.pressButton((widget.profile.avatar != null) ? true : false);
                          },
                        ),
                        SizedBox(width: 10,),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 23,),
                            Text(
                              'Hello!',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 24,
                                fontWeight: FontWeight.w500,
                                fontFamily: 'Roboto',
                                height: 30/24
                              ),
                            ),
                            Text(
                              'We trust you are having a great time',
                              style: TextStyle(
                                color: Color(0xFFCFCFCF),
                                fontSize: 12,
                                fontWeight: FontWeight.w400,
                                  fontFamily: 'Roboto',
                                  height: 16/12
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                    SvgPicture.asset('assets/not.svg'),
                    SizedBox(width: 10,)
                  ],
                ),
              ),
              SizedBox(height: 35,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,

                children: [
                  Text(
                    "Special for you",
                    style: TextStyle(
                        color: Color(0xFFEC8000),
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'Roboto',
                        height: 16/14
                    ),
                  ),
                  InkWell(
                    child: SvgPicture.asset('assets/orange.svg'),
                    onTap: (){
                      final rightOffset = controller.position.maxScrollExtent;
                      controller.animateTo(rightOffset, duration: Duration(milliseconds: 1000), curve: Curves.easeInOut,);
                    },
                  )
                ],
              ),
              SizedBox(height: 7,),]))),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 24),

              child: Container(
                height: 64,
                child: ListView.separated(
                  separatorBuilder: (_, index) {return SizedBox(width: 12,);},
                    itemCount: adsUrl.length,
                    controller: controller,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (_, index){
                      return ClipRRect(
                        borderRadius: BorderRadius.circular(8),
                        child: Image.network(adsUrl[index]),
                      );
                    }),
              ),)),
              SliverToBoxAdapter(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 19,),
                      Text("What would you like to do",
                        style: TextStyle(
                            color: Color(0xFF0560FA),
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                        height: 16/14,
                        fontFamily: 'Roboto'),),
                      SizedBox(height: 9,),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          InkWell(
                            child: Container(
                                width: (widthScreen - 71) / 2,
                                height: (widthScreen - 71) / 2,
                                decoration: BoxDecoration(
                                    color: (focus == 1) ? Color(0xFF0560FA):Color(0xFFF2F2F2),
                                    borderRadius: BorderRadius.circular(8),
                                    boxShadow: [
                                      BoxShadow(
                                          color: Color(0x26000026),
                                          blurRadius: 5,
                                          offset: Offset(0, 2)
                                      )
                                    ]
                                ),
                                child: Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 8),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(height: 26.5,),
                                      SvgPicture.asset('assets/person.svg', color: (focus == 1)?Colors.white:Color(0xFF0560FA),),
                                      SizedBox(height: 6,),
                                      Text("Customer Care",
                                          style: TextStyle(
                                              color: (focus == 1)?Colors.white:Color(0xFF0560FA),
                                              fontSize: 16, fontWeight: FontWeight.w500
                                          )

                                      ),
                                      SizedBox(height: 6,),
                                      Text(
                                        "Our customer care service line is \navailable from 8 -9pm week days and \n9 - 5 weekends - tap to call us today",
                                        style: TextStyle(
                                            color: (focus == 1)?Colors.white:Color(0xFF3A3A3A),
                                            fontSize: 7.45, fontWeight: FontWeight.w400
                                        ),
                                      )
                                    ],
                                  ),
                                )
                            ),
                            onTap: (){
                              setState(() {
                                focus = 1;
                              });
                            },

                          ),
                          SizedBox(width: 23,),
                          InkWell(
                            child: Container(
                                width: (widthScreen - 71) / 2,
                                height:(widthScreen - 71) / 2,
                                decoration: BoxDecoration(
                                    color: (focus == 2) ? Color(0xFF0560FA):Color(0xFFF2F2F2),
                                    borderRadius: BorderRadius.circular(8),
                                    boxShadow: [
                                      BoxShadow(
                                          color: Color(0x26000026),
                                          blurRadius: 5,
                                          offset: Offset(0, 2)
                                      )
                                    ]
                                ),
                                child: Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 8),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(height: 26.5,),
                                      SvgPicture.asset('assets/cube.svg', color: (focus == 2)?Colors.white:Color(0xFF0560FA),),
                                      SizedBox(height: 6,),
                                      Text("Send a package",
                                          style: TextStyle(
                                              color: (focus == 2)?Colors.white:Color(0xFF0560FA),
                                              fontSize: 16, fontWeight: FontWeight.w500,fontFamily: 'Roboto',

                                          )

                                      ),
                                      SizedBox(height: 6,),
                                      Text(
                                        "Request for a driver to pick up or \ndeliver your package for you",
                                        style: TextStyle(
                                            color: (focus == 2)?Colors.white:Color(0xFF3A3A3A),
                                            fontSize: 7.45,
                                            fontWeight: FontWeight.w400,
                                            fontFamily: 'Roboto',
                                            height: 30/22.35
                                        ),
                                      )
                                    ],
                                  ),
                                )
                            ),
                            onTap: (){
                              setState(() {
                                focus = 2;
                              });
                            },
                          ),
                        ],
                      ),
                      SizedBox(height: 23,),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          InkWell(
                            child: Container(
                                width: (widthScreen - 71) / 2,
                                height: (widthScreen - 71) / 2,
                                decoration: BoxDecoration(
                                    color: (focus == 3) ? Color(0xFF0560FA):Color(0xFFF2F2F2),
                                    borderRadius: BorderRadius.circular(8),
                                    boxShadow: [
                                      BoxShadow(
                                          color: Color(0x26000026),
                                          blurRadius: 5,
                                          offset: Offset(0, 2)
                                      )
                                    ]
                                ),
                                child: Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 8),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(height: 26.5,),
                                      SvgPicture.asset('assets/money.svg', color: (focus == 3)?Colors.white:Color(0xFF0560FA),),
                                      SizedBox(height: 6,),
                                      Text("Fund your wallet",
                                          style: TextStyle(
                                              color: (focus == 3)?Colors.white:Color(0xFF0560FA),
                                              fontSize: 16, fontWeight: FontWeight.w500, fontFamily: 'Roboto',

                                          )

                                      ),
                                      SizedBox(height: 6,),
                                      Text(
                                        "To fund your wallet is as easy as ABC,\n make use of our fast technology and \ntop-up your wallet today",
                                        style: TextStyle(
                                            color: (focus == 3)?Colors.white:Color(0xFF3A3A3A),
                                            fontSize: 7.45, fontWeight: FontWeight.w400,fontFamily: 'Roboto',
                                            height: 30/22.35
                                        ),
                                      )
                                    ],
                                  ),
                                )
                            ),
                            onTap: (){
                              setState(() {
                                focus = 3;
                              });
                            },
                          ),
                          SizedBox(width: 23,),
                          InkWell(
                            child: Container(
                                width: (widthScreen - 71) / 2,
                                height: (widthScreen - 71) / 2,
                                decoration: BoxDecoration(
                                    color: (focus == 4) ? Color(0xFF0560FA):Color(0xFFF2F2F2),
                                    borderRadius: BorderRadius.circular(8),
                                    boxShadow: [
                                      BoxShadow(
                                          color: Color(0x26000026),
                                          blurRadius: 5,
                                          offset: Offset(0, 2)
                                      )
                                    ]
                                ),
                                child: Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 8),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(height: 26.5,),
                                      SvgPicture.asset('assets/chats.svg', color: (focus == 4)?Colors.white:Color(0xFF0560FA),),
                                      SizedBox(height: 6,),
                                      Text("Chats",
                                          style: TextStyle(
                                              color: (focus == 4)?Colors.white:Color(0xFF0560FA),
                                              fontSize: 16, fontWeight: FontWeight.w500, fontFamily: 'Roboto',

                                          )

                                      ),
                                      SizedBox(height: 6,),
                                      Text(
                                        "Search for available rider within your \narea",
                                        style: TextStyle(
                                            color: (focus == 4)?Colors.white:Color(0xFF3A3A3A),
                                            fontSize: 7.45, fontWeight: FontWeight.w400, fontFamily: 'Roboto',
                                            height: 30/22.35
                                        ),
                                      )
                                    ],
                                  ),
                                )
                            ),
                            onTap: (){
                              setState(() {
                                focus = 4;
                              });
                              Navigator.push(context, MaterialPageRoute(builder: (context) => Chats(profile: widget.profile))).then((value) => setState(() {

                              }));
                            },

                          ),
                        ],
                      ),
                      SizedBox(height: 45)
                    ],
                  ),
                ),
              )


            ],
          ),

    ):Center(child: CircularProgressIndicator(),);
  }
}
